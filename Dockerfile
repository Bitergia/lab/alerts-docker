FROM python:2

MAINTAINER "Luis Cañas-Díaz <lcanas@bitergia.com>"

RUN pip install elastalert
RUN apt-get install git

COPY entrypoint /

CMD ["/entrypoint"]
